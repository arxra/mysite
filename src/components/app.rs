use leptos::prelude::*;
use leptos_router::components::*;
use leptos_router::path;

use crate::components::compiler::Compiler;
use crate::components::home::Home;

#[component]
pub fn App() -> impl IntoView {
    view! {
        <div class="justify-start h-screen bg-sky-300 dark:bg-sky-950">
            <Router>
                <nav class="flex sticky top-0 w-full text-black dark:text-white bg-sky-400 start-0 dark:bg-sky-900">
                    <div class="flex items-center mx-auto max-w-screen-xl">
                        <NavItem href="/">"Home"</NavItem>
                        <NavItem href="/compiler">"Compiler"</NavItem>
                        <NavItem href="https://www.gitlab.com/arxra/mysite">"Site Source"</NavItem>
                    </div>
                </nav>
                <main class="container mx-auto h-5/6">
                    <Routes fallback=|| "Not found.">
                        <Route path=path!("/") view=Home/>
                        <Route path=path!("/compiler") view=Compiler/>
                    </Routes>
                </main>
            </Router>
        </div>
    }
}

#[component]
fn NavItem(href: &'static str, children: Children) -> impl IntoView {
    view! {
        <a
            href=href
            class="block py-2 px-3 rounded border-sky-500 dark:hover:bg-sky-700 hover:bg-sky-300"
        >
            {children()}
        </a>
    }
}
