use leptos::prelude::*;

use crate::components::link::Link;

#[component]
pub fn home() -> impl IntoView {
    view! {
        <div class="container flex mx-auto max-w-2xl dark:text-white">
            <div class="flex flex-col gap-4 justify-items-center p-4 md:gap-6">
                <h2 class="flex justify-center text-4xl">"Hello there!"</h2>
                <div class="flex">
                    <p>
                        "This space is a collection of public work, random thoughts and various other collections. "
                        "For all intents and purposes, its more of a easily shared CV than anything, sporting a "
                        <Link href="/compiler">
                            "c-compiler to x86 written in rust and compile to WebAssembly "
                        </Link> "as a example."
                        "What is missing is a more detailed view of previous work and eductaion as you would find on a more traditional CV, "
                        <Link href="https://gitlab.com/arxra/mysite/-/issues/8">"WIP"</Link>
                    </p>
                </div>
                <div class="flex-row">
                    <p>
                        "But why make your own site from scratch?" <br/>
                        "Why not just use a site generator and be done with it?"
                    </p>
                    <h3 class="font-semibold text-xl m-4">"In philosophical terms"</h3>
                    <p>
                        "That would indeed have solved the above paragraphs goals and save a lot of time.
                        There is one more part to doing it from scratch which goes into my philosophy in development:
                        You should know enough about a subject before you write it off.
                        I like to view myself as a platform/backend engineer, not a frontend one.
                        However, its hard to argue _why_ I do not like frontend if I do not have anything to show for it.
                        So, while this site might attempt to get you to hire me in the forementioned roles its also a detterant
                        for frontend positions and a message to myself that \"a sufficient amount of frontend\" effort has been 
                        dispelled."
                    </p>
                    <h3 class="font-semibold text-xl m-4">"In technical terms"</h3>
                    <p>
                        "This site is just some basic WASM components built with "
                        <Link href="https://leptos.dev/">"Leptos"</Link> " and stylized with "
                        <Link href="https://tailwindcss.com/">"TailwindCSS"</Link>
                        ". There is a functioning router and basic reactivity demonstrated with the above mentioned small compiler.
                        There is probalby better ways of styling a website but I have no clue how make gaps and tailwind color nuances work toghether.
                        If you're visiting with light mode, then it will probaly look even worse as the site is dark-mode first."
                    </p>
                </div>
            </div>
        </div>
    }
}
