use leptos::prelude::*;

#[component]
pub fn link(children: Children, href: &'static str) -> impl IntoView {
    view! {
        <a
            href=href
            class="hover:underline text-sky-800 dark:text-sky-200 dark:hover:text-sky-300 hover:text-sky-500"
        >
            {children()}
        </a>
    }
}
