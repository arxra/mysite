use cigrid::compile;
use leptos::prelude::*;

#[component]
pub fn compiler() -> impl IntoView {
    let default_c = "
#include <stdio.h>

extern int putchar(int x);

int main() {

  int x = 1;
  int y = 5;
  x = x + 4;
  x = x + y;
  putchar('A');
  putchar(x);
  return 0;
}"
    .to_string();
    let (c, set_c) = signal("".to_owned());
    let x86 = move || match compile(c.get()) {
        Ok(a) => a,
        Err(e) => {
            leptos::logging::error!("{}", e);
            format!("Invalid c:\n {}", e)
        }
    };

    view! {
        <div class="h-full">
            <h2 class="p-4 text-4xl font-medium dark:text-white">"Compiler"</h2>
            <div class="flex flex-col gap-4 justify-between h-full md:flex-row">
                <textarea
                    class="flex order-1 p-1 w-full h-2/3 text-left resize-none md:w-2/5 md:h-full"
                    on:input=move |ev| set_c.set(event_target_value(&ev))
                >
                    {c}
                </textarea>
                <div class="flex flex-col justify-items-center md:order-1 md:gap-4 md:w-1/5 -order-1">
                    <button
                        on:click=move |_| set_c.set(default_c.clone())
                        class="justify-center py-2 px-3 w-full font-medium text-center text-black rounded-lg dark:text-white focus:ring-4 focus:outline-none bg-sky-500 dark:bg-sky-650 dark:hover:bg-sky-600 dark:focus:ring-sky-800 hover:bg-sky-800 focus:ring-sky-200"
                    >
                        "Use c example"
                    </button>
                    <div class="flex flex-col gap-4 justify-between p-2 md:order-1 dark:text-white -order-1">
                        <a
                            class="flex hover:underline text-sky-800 dark:text-sky-200 dark:hover:text-sky-300 hover:text-sky-500"
                            href="https://gitlab.com/arxra/my-cigrid-compiler"
                        >
                            "The source for this compiler is available at GitLab"
                        </a>
                        <p class="flex">
                            "This compiler was written as part of ID2202 at Kungliga Tekniska Högskolan in Stockholm.
                            It just managed a passing grade, as there are many things missing but its a cool demonstration of Rust and Wasm.
                            Yes, I would have liked to earn a higher grade, but it turns out learning rust and how to write a compiler in the same 10 weeks is really hard.
                            Not to mention, that was just one of the courses in that period... Yes I'm making excuses here.
                            I learned a lot during this project about Rust and compilers, so there _should_ be a blogpost on this site about it.
                            Should. In the meantime, see the readme of the compiler."
                        </p>
                    </div>
                </div>
                <textarea
                    class="flex order-1 p-1 w-full h-2/3 text-left resize-none md:w-2/5 md:h-full"
                    readonly
                >
                    {x86}
                </textarea>
            </div>
        </div>
    }
}
