use leptos::prelude::*;

mod components;

fn main() {
    console_error_panic_hook::set_once();
    mount_to_body(components::app::App)
}
