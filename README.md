# My Site

[https://aron.rs](https://aron.rs)

Managed to snagg a good dns in time, very najs!

## Building

Run

```sh
trunk build --release
```

Trunk is configured to compile the CSS (TailWindCSS) and minify it.
NOTE: You do need `trunk`, `npx` and `nightly rust` available to run.

## Formatting

The formatting is a combination of Rust-fmt and [rustywind](https://github.com/avencera/rustywind) for tailwind CSS-classes.

## Packaging

Since this is meant to be deployed to K8s make sure it is built in a container and for amd64,

```bash
docker build --platform linux/amd64 . -t $REPO_NAME

```

## Why?

Why build yet another about me site when there are a significant amount of "me blog me here yes" sites as is?
Well, how many of those send a SPA with a [c-subset compiler](https://gitlab.com/arxra/my-cigrid-compiler) within? Not many!
Is that really necessary though? Absolutely not! But it was fun seeing how one project integrates into your next (even if one of them is obligatory uni work).

## Rewrite?

This project will probably get rewritten before this document so do ask Aron about changes.

## Spoilers

Got the compiler working!

![a picture of the site](./static/1627128923_screenshot.png)
