FROM rust:1.84-bookworm AS build

WORKDIR /usr/src/myapp

RUN curl -L --proto '=https' --tlsv1.2 -sSf https://raw.githubusercontent.com/cargo-bins/cargo-binstall/main/install-from-binstall-release.sh | bash
RUN cargo binstall  trunk && rustup target add wasm32-unknown-unknown
RUN curl -fsSL https://deb.nodesource.com/setup_22.x | bash - && \
  apt-get install -y nodejs && \
  bash -c "npm install -D tailwindcss@3"

COPY . .

RUN bash -c "npx tailwindcss -i input.css -o style/output.css --minify" &&\
  trunk build --release

FROM nginx:1.27.3-alpine-otel
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /usr/src/myapp/dist /usr/share/nginx/html
